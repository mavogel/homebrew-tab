# homebrew-taps
for testing https://gitlab.com/mavogel/release-testing

## Installing
```sh
#brew tap mavogel/tools <git-clone-url>
brew tap mavogel/tools git@gitlab.com:mavogel/homebrew-tab.git
brew install release-testing
# or if you have a naming conflict
# see https://wheniwork.engineering/creating-a-private-homebrew-tap-with-gitlab-8800c453d893
# brew install mavogel/tools/release-testing
```

## Updating
Each time you run `brew update` it also gets the updates from the taps
```sh
brew update
brew upgrade # to perform the actual updates
```